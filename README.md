# Garden Center API Project v7

***


## Name
Module 4 - Garden Center API Project v7

## Author
[Donald Moore](https://gitlab.com/dondev1992)

## Description
To build an api and database using layered architecture that allows full CRUD and custom requests for a garden center web application. Users are able to make access information stored in a postgres database. Unit and integration tests were created to check the functionality and cohesiveness of the application. Logging dependancies were added to log the behavoir of the application during use.

## Clone the project
Clone the project by clicking on the clone icon in this repository and copy the https address. Open your IDE of choice and click on the VCS clone option. Paste the https address into your IDE and open the project.

## Installation
For installation, please have the latest version of PostgresSQL installed and confirm the application.yml file in the resource folder is updated as followed:
```
spring:
  jpa:
    database: POSTGRESQL
    show-sql: true
    hibernate:
      ddl-auto: create-drop
  datasource:
    platform: postgres
    url: jdbc:postgresql://localhost:5432/postgres
    username: postgres
    password: postgres
    driverClassName: org.postgresql.Driver
```
## Start the API and Databse connection

First, start your server using a GUI like pgAdmin. Be sure that your database is up an running.

Next, open the AppRunner class in Intellij or equivalent platform and click on the green run button to the left of the AppRunner class title to run the file. SpringBoot should run successfully and the data will populate the Users, Customers, Products, and Orders tables once the server starts. If there are any issues with the dependencies, please open the pom.xml file and confirm the dependencies are updated.
Next, please import the postman collection via the [attached link provided](https://www.postman.com/orange-trinity-452585/workspace/garden-center-api/collection/18114134-cc43a5ba-abc4-40fe-bbdb-b5f5502d6c2e?action=share&creator=18114134) and run the requests. The request listings can be autorun or individually selected and ran.

## Postman Collection Link
https://www.postman.com/orange-trinity-452585/workspace/garden-center-api/collection/18114134-cc43a5ba-abc4-40fe-bbdb-b5f5502d6c2e?action=share&creator=18114134

## Using Postman
After you have open the link provided in Postman, go to the folder of you choice and select a request method by clicking on it. Simply click on the run button to start the request.

## How to run the Unit and Integration tests
Locate the test folder inside the src folder of the project. Open the java>io.catalyte.training folders to get to the services folder for unit testing and the controllers folder for integration testing. To run all tests at once, right click on the io.catalyte.training folder and select Run "Tests in 'io.catalyte.training'" or select Run "Tests in 'io.catalyte.training'" with Coverage. To run each test separately, open either services or controllers folder, open a file to test. Then click the green run button to the left of the individual method that you want to test.

## Support
Please like, subscribe, and comment below if you have any questions or would like to provide any feedback.


## Contributing
The project is open to contributions and you must be part of the Create Opportunity program.


## Authors and acknowledgment
Don Moore and Richy Phongsavath are authors of this project as part of the Crate Opportunity program at Central Piedmont Community College.

## License
This application is open sourced and stored in a public GitLab repository.

## Project status
The project is completed unless specifically stated by review. 
