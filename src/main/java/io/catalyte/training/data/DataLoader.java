package io.catalyte.training.data;


import io.catalyte.training.entities.*;
import io.catalyte.training.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private ProductsRepository productsRepository;
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ItemsRepository itemsRepository;

    private Customers customer1;
    private Customers customer2;
    private Customers customer3;
    private Customers customer4;
    private Customers customer5;
    private Customers customer6;

    private Products product1;
    private Products product2;
    private Products product3;
    private Products product4;
    private Products product5;
    private Products product6;

    private final Logger logger = LoggerFactory.getLogger(DataLoader.class);

    @Override
    public void run(String... args) throws Exception {
        logger.info("Loading data...");

        loadUsers();
        loadCustomers();
        loadProducts();
        loadOrders();


    }

    private void loadProducts() {
        product1 = productsRepository.save(new Products(495837, "safety", "gloves", "Easy to use, and don't sweat hands.", "Bully Tools", new BigDecimal("12.49")));
        product2 = productsRepository.save(new Products(499432, "tool", "shears", "Self sharpening blades.", "Lowes", new BigDecimal("17.99")));
        product3 = productsRepository.save(new Products(496330, "tool", "loppers", "Made in the USA.", "Stoller", new BigDecimal("26.99")));
        product4 = productsRepository.save(new Products(497432,"tool", "fork", "Extra reinforced handles.", "Bully Tools", new BigDecimal("22.79")));
        product5 = productsRepository.save(new Products(498835, "tool", "spade", "Durable, ergonomic design", "Stoller", new BigDecimal("12.49")));
        product6 = productsRepository.save(new Products(497212, "cleanup", "lawn bags", "Large, heavy duty lawn bags.", "Lowes", new BigDecimal("15.59")));
    }

    private void loadCustomers() {
        customer1 = customersRepository.save(new Customers("Churina Sa", "churina@email.com", new Address("123 Tryon St.",  "Charlotte", "NC", 28215)));
        customer2 = customersRepository.save(new Customers("Will Boese", "will@email.com", new Address("4652 Brevard St.", "Charlotte", "NC", 28215)));
        customer3 = customersRepository.save(new Customers("Jazmarie Montcrief", "jazmarie@email.com", new Address("2023 Trade St.", "Charlotte", "NC", 28269)));
        customer4 = customersRepository.save(new Customers("Richy Phongsavath", "richy@email.com", new Address("3745 Independence Blvd.", "Matthews", "NC", 28227)));
        customer5 = customersRepository.save(new Customers("DI von Breisen", "di@email.com", new Address("987 Graham St.", "Charlotte", "NC", 28364)));
        customer6 = customersRepository.save(new Customers("Jared Dillahunt", "jared@email.com", new Address("110 South Blvd.", "Charleston", "SC", 26789)));
    }

    private void loadUsers() {
        usersRepository.save(new Users("Rachel Monroe", "Cashier", List.of("EMPLOYEE"), "rachel@email.com", "happyDays2023"));
        usersRepository.save(new Users("Michael Moore", "Lead Programmer", List.of("EMPLOYEE"), "michael@email.com", "liveLife09"));
        usersRepository.save(new Users("Kevin Costner", "Manager", List.of("ADMIN"), "kevin@email.com", "28DaysOfNight"));
        usersRepository.save(new Users("Austin Kutchner", "Cashier", List.of("EMPLOYEE"), "austin@email.com", "woWhA843fjt344"));
        usersRepository.save(new Users("Kynna Moore", "Project Manager", List.of("ADMIN"), "kynna@email.com", "HamburgerLover101"));
        usersRepository.save(new Users("Kevin Dawson", "Manager", List.of("ADMIN"), "kevin@email.com", "IshOttHesHeRiFf1972"));
    }

    private void loadOrders() {
        Items item1 = itemsRepository.save(new Items(product6, 4));
        Items item2 = itemsRepository.save(new Items(product5, 2));
        Items item3 = itemsRepository.save(new Items(product4, 8));
        Items item4 = itemsRepository.save(new Items(product3, 12));
        Items item5 = itemsRepository.save(new Items(product2, 1));
        Items item6 = itemsRepository.save(new Items(product1, 5));
        Items item7 = itemsRepository.save(new Items(product6, 4));
        Items item8 = itemsRepository.save(new Items(product5, 2));
        Items item9 = itemsRepository.save(new Items(product4, 8));
        Items item10 = itemsRepository.save(new Items(product3, 12));
        Items item11 = itemsRepository.save(new Items(product2, 10));
        Items item12 = itemsRepository.save(new Items(product6, 5));

        List<Items> itemList1 = new ArrayList<>(List.of(item1, item2, item3));
        List<Items> itemList2 = new ArrayList<>(List.of(item4, item5, item6));
        List<Items> itemList3 = new ArrayList<>(List.of(item7, item8, item10));
        List<Items> itemList4 = new ArrayList<>(List.of(item11, item12, item9));

        Orders order1 = ordersRepository.save(new Orders(customer1, Date.from(
                LocalDate.parse("2022-10-17").atStartOfDay(ZoneId.systemDefault()).toInstant())
        ));
        order1.setItems(itemList1);
        ordersRepository.save(order1);

        Orders order2 = ordersRepository.save(new Orders(customer2, Date.from(
                LocalDate.parse("2022-12-13").atStartOfDay(ZoneId.systemDefault()).toInstant())
        ));
        order2.setItems(itemList2);
        ordersRepository.save(order2);

        Orders order3 = ordersRepository.save(new Orders(customer3, Date.from(
                LocalDate.parse("2022-12-15").atStartOfDay(ZoneId.systemDefault()).toInstant())
        ));
        order3.setItems(itemList3);
        ordersRepository.save(order3);

        Orders order4 = ordersRepository.save(new Orders(customer4, Date.from(
                LocalDate.parse("2022-10-17").atStartOfDay(ZoneId.systemDefault()).toInstant())
        ));
        order4.setItems(itemList4);
        ordersRepository.save(order4);
    }
}
