package io.catalyte.training.customExceptions;

public class BadDataResponse extends RuntimeException {

    public BadDataResponse() {
    }

    public BadDataResponse(String message) {
        super(message);
    }
}

