package io.catalyte.training.customExceptions;

public class ServerError extends RuntimeException {

    public ServerError() {
    }

    public ServerError(String message) {
        super(message);
    }
}

