package io.catalyte.training.repositories;

import io.catalyte.training.entities.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {

    List<Orders> findByCustomersId(Long customers_id);

    List<Orders> findByItems_ProductsId(Long product_id);

    List<Orders> findByItems_Quantity(Integer quantity);

}
