package io.catalyte.training.repositories;

import io.catalyte.training.entities.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Long> {

    List<Customers> findByAddressStreet(String street);

    List<Customers> findByAddressCity(String city);

    List<Customers> findByAddressState(String state);

    List<Customers> findByAddressZipCode(Integer zipCode);

    boolean existsByEmail(String email);
}
