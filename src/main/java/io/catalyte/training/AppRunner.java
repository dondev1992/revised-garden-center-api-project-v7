/**
 * @version - Garden Center API Project v7
 * @task - To build an api and database using layered architecture that allows full CRUD and custom requests for a garden center web application.
 * Users are able to make access information stored in a postgres database.
 * Unit and integration tests were created to check the functionality and cohesiveness of the application. Logging dependencies were added to log the behavior of the application during use.
 * @author - Donald Moore
 */

package io.catalyte.training;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRunner {
    private static Logger logger = LoggerFactory.getLogger(AppRunner.class);

    public static void main(String[] args) {
        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.info("This is a info message");
        logger.warn("This is a warn message");
        logger.error("This is a error message");

        SpringApplication.run(AppRunner.class);
    }
}
