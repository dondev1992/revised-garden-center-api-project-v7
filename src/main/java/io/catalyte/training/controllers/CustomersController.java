package io.catalyte.training.controllers;

import io.catalyte.training.entities.Customers;
import io.catalyte.training.services.CustomersServices;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static io.catalyte.training.constants.Constants.*;

@RestController
@RequestMapping(value = "/customers")
public class CustomersController {
    private final Logger logger = LoggerFactory.getLogger(CustomersController.class);

    @Autowired
    private CustomersServices customersServices;

    @GetMapping
    public ResponseEntity<List<Customers>> getAllCustomers(Customers customer) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + customer.toString());

        return new ResponseEntity<>(customersServices.getAllCustomers(customer), HttpStatus.OK);
    }

    @GetMapping(value = "/street")
    public ResponseEntity<List<Customers>> getAllCustomersByStreet(@Valid @RequestParam String street) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(customersServices.getCustomersByStreet(street), HttpStatus.OK);
    }

    @GetMapping(value = "/city")
    public ResponseEntity<List<Customers>> getAllCustomersByCity(@Valid @RequestParam String city) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(customersServices.getCustomersByCity(city), HttpStatus.OK);
    }

    @GetMapping(value = "/state")
    public ResponseEntity<List<Customers>> getAllCustomersByState(@Valid @RequestParam String state) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(customersServices.getCustomersByState(state), HttpStatus.OK);
    }

    @GetMapping(value = "/zipcode")
    public ResponseEntity<List<Customers>> getAllCustomersByZipCode(@Valid @RequestParam Integer zipCode) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(customersServices.getCustomersByZipCode(zipCode), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Customers> getCustomerById(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + id);

        return new ResponseEntity<>(customersServices.getCustomerById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Customers> addCustomer(@Valid @RequestBody Customers customer) {
        logger.info(new Date() + LOGGER_POST_REQUEST_RECEIVED);

        return new ResponseEntity<>(customersServices.addCustomer(customer), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Customers> updateCustomersById(
            @PathVariable Long id, @Valid @RequestBody Customers customer) {
        logger.info(new Date() + LOGGER_PUT_REQUEST_RECEIVED + id);

        return new ResponseEntity<>(customersServices.updateCustomerById(id, customer), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED + id);

        customersServices.deleteCustomer(id);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllCustomers() {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED);

        customersServices.deleteAllCustomers();
    }
}
