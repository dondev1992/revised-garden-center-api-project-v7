package io.catalyte.training.controllers;

import io.catalyte.training.entities.Products;
import io.catalyte.training.services.ProductsServices;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static io.catalyte.training.constants.Constants.*;


@RestController
@RequestMapping(value = "/products")
public class ProductsController {
    private final Logger logger = LoggerFactory.getLogger(ProductsController.class);

    @Autowired
    private ProductsServices productsServices;

    @GetMapping
    public ResponseEntity<List<Products>> getAllProducts(Products product) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + product.toString());

        return new ResponseEntity<>(productsServices.getAllProducts(product), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Products> getProductById(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + id);

        return new ResponseEntity<>(productsServices.getProductById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Products> addProduct(@Valid @RequestBody Products product) {
        logger.info(new Date() + LOGGER_POST_REQUEST_RECEIVED);

        return new ResponseEntity<>(productsServices.addProduct(product), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Products> updateProductsById(
            @PathVariable Long id, @Valid @RequestBody Products product) {
        logger.info(new Date() + LOGGER_PUT_REQUEST_RECEIVED);

        return new ResponseEntity<>(productsServices.updateProductById(id, product), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED + id);

        productsServices.deleteProduct(id);
    }
}
