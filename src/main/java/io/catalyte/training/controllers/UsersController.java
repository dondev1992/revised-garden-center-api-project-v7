package io.catalyte.training.controllers;

import io.catalyte.training.entities.Users;
import io.catalyte.training.services.UsersServices;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static io.catalyte.training.constants.Constants.*;

@RestController
@RequestMapping(path = "/users")
public class UsersController {

    private final Logger logger = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UsersServices usersServices;

    @GetMapping
    public ResponseEntity <List<Users>> getAllUsers(Users user) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + user.toString());

        return new ResponseEntity<>(usersServices.getAllUsers(user), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Users> getUserById(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + id);

        return new ResponseEntity<>(usersServices.getUserById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Users> addUser(@Valid @RequestBody Users user) {
        logger.info(new Date() + LOGGER_POST_REQUEST_RECEIVED);

        return new ResponseEntity<>(usersServices.addUser(user), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Users> updateUsersById(
            @PathVariable Long id, @Valid @RequestBody Users user) {
        logger.info(new Date() + LOGGER_PUT_REQUEST_RECEIVED);

        return new ResponseEntity<>(usersServices.updateUserById(id, user), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED + id);
        usersServices.deleteUser(id);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllUsers() {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED);
        usersServices.deleteAllUsers();
    }

}
