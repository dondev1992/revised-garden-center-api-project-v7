package io.catalyte.training.controllers;

import io.catalyte.training.entities.Orders;
import io.catalyte.training.services.OrdersServices;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static io.catalyte.training.constants.Constants.*;

@RestController
@RequestMapping(value = "/orders")
public class OrdersController {
    private final Logger logger = LoggerFactory.getLogger(OrdersController.class);

    @Autowired
    private OrdersServices ordersServices;

    List<Orders> ordersList = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<Orders>> getAllOrders(Orders orders) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + orders.toString());

        return new ResponseEntity<>(ordersServices.getAllOrders(orders), HttpStatus.OK);
    }

    @GetMapping(value = "/customer")
    public ResponseEntity<List<Orders>> getOrdersByCustomerId(@Valid @RequestParam Long customerId) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(ordersServices.getAllOrdersByCustomerId(customerId), HttpStatus.OK);
    }

    @GetMapping(value = "/product")
    public ResponseEntity<List<Orders>> getAllOrdersByProductId(@Valid @RequestParam Long productId) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(ordersServices.getAllOrdersByProductId(productId), HttpStatus.OK);
    }

    @GetMapping(value = "/quantity")
    public ResponseEntity<List<Orders>> getAllOrdersByQuantity(@Valid @RequestParam Integer quantity) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED);

        return new ResponseEntity<>(ordersServices.getAllOrdersByQuantity(quantity), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Orders> getOrderById(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_REQUEST_RECEIVED + id);

        return new ResponseEntity<>(ordersServices.getOrderById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Orders> addOrder(@Valid @RequestBody Orders order) {
        logger.info(new Date() + LOGGER_POST_REQUEST_RECEIVED);

        return new ResponseEntity<>(ordersServices.addOrder(order), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Orders> updateOrdersById(
            @PathVariable Long id, @Valid @RequestBody Orders order) {
        logger.info(new Date() + LOGGER_PUT_REQUEST_RECEIVED);

        return new ResponseEntity<>(ordersServices.updateOrderById(id, order), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrder(@PathVariable Long id) {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED + id);

        ordersServices.deleteOrder(id);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllOrders() {
        logger.info(new Date() + LOGGER_DELETE_REQUEST_RECEIVED);

        ordersServices.deleteAllOrders();
    }
}


