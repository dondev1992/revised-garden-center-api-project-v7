package io.catalyte.training.services;

import io.catalyte.training.entities.Products;

import java.util.List;

public interface ProductsServices {

    List<Products> getAllProducts(Products product);

    Products getProductById(Long id);

    Products addProduct(Products product);

    Products updateProductById(Long id, Products product);

    void deleteProduct(Long id);

}
