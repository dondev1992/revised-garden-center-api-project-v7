package io.catalyte.training.services;

import io.catalyte.training.entities.Orders;

import java.util.List;

public interface OrdersServices {

    List<Orders> getAllOrders(Orders Orders);

    List<Orders> getAllOrdersByProductId(Long productId);

    List<Orders> getAllOrdersByQuantity(Integer quantity);

    List<Orders> getAllOrdersByCustomerId(Long customer_id);

    Orders getOrderById(Long id);

    Orders addOrder(Orders order);

    Orders updateOrderById(Long id, Orders order);

    void deleteOrder(Long id);

    void deleteAllOrders();

}
