package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.Conflict;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Products;
import io.catalyte.training.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductsServicesImplements implements ProductsServices{

    @Autowired
    private ProductsRepository productsRepository;

    @Override
    public List<Products> getAllProducts(Products product) {
        try {
            if (product.equals(null)) {
                return productsRepository.findAll();
            } else {
                Example<Products> productExample = Example.of(product);
                return productsRepository.findAll(productExample);
            }
        } catch ( Exception e) {
            throw new ServiceUnavailable(e);
        }
    }

    @Override
    public Products getProductById(Long id) {
        try {
            Products product = productsRepository.findById(id).orElse(null);

            if (product != null) {
                return product;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the Product
        throw new ResourceNotFound("Could not locate a Product with the id: " + id);

    }

    @Override
    public Products addProduct(Products product) {
        if (!isParameterSkuEqualToProductSku(product)) {
            try {
                return productsRepository.save(product);
            } catch (Exception e) {
                throw new ServiceUnavailable(e);
            }
        }
        throw new Conflict("This Product sku is already in use");
    }

    @Override
    public Products updateProductById(Long id, Products product) {
        if (!product.getId().equals(id)) {
            throw new BadDataResponse("Product id must match the id specified in the URL");
        }
//        if (isPathIdEqualToExistingProductId(product)) {
//            throw new ResourceNotFound("Could not locate a Product with the id: 1");
//        }
        List<Products> productsList = productsRepository.findAll();
        for (Products productsDb : productsList) {
            if (productsDb.getId().equals(product.getId())) {
                if (!productsDb.getId().equals(product.getId())) {
                    throw new Conflict("Product address already exists");
                }
            }
        }
        try {
            Products productFromDB = productsRepository.findById(id).orElse(null);

            if (productFromDB != null) {
                return productsRepository.save(product);
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
        // if we made it down to this pint, we did not find the Product
        throw new ResourceNotFound("Could not locate a Product with the id: " + id);
    }

    @Override
    public void deleteProduct(Long id) {

        try {
            if (productsRepository.existsById(id)) {
                productsRepository.deleteById(id);
                return;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the Product
        throw new ResourceNotFound("Could not locate a Product with the id: " + id);
    }

    public boolean isParameterSkuEqualToProductSku(Products product) {
        List<Products> productsList = productsRepository.findAll();
        for (Products productsDb : productsList) {
            if (productsDb.getSku().equals(product.getSku())) {
                return true;
            }
        }
        return false;
    }

}
