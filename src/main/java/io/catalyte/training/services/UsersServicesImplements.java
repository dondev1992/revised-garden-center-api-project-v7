package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.Conflict;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Users;
import io.catalyte.training.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServicesImplements implements UsersServices{

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<Users> getAllUsers(Users user) {
        try {
            if (user.equals(null)) {
                return usersRepository.findAll();
            } else {
                Example<Users> userExample = Example.of(user);
                return usersRepository.findAll(userExample);
            }
            } catch (Exception e) {
                throw new ServiceUnavailable(e);
            }

    }

    @Override
    public Users getUserById(Long id) {
        try {
            Users user = usersRepository.findById(id).orElse(null);

            if (user != null) {
                return user;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the User
        throw new ResourceNotFound("Could not locate a User with the id: " + id);

    }

    @Override
    public Users addUser(Users user) {
        if (usersRepository.existsByEmail(user.getEmail())) {
            throw new Conflict("Email address already exists");
        }
            try {
                return usersRepository.save(user);
            } catch (Exception e) {
                throw new ServiceUnavailable(e);
            }
    }

    @Override
    public Users updateUserById(Long id, Users user) {
        if (!user.getId().equals(id)) {
            throw new BadDataResponse("User id must match the id specified in the URL");
        }
        if (!isPathIdEqualToExistingUserId(user)) {
            throw new ResourceNotFound("User id: " + id + " does not exist");
        }
        List<Users> usersList = usersRepository.findAll();
        for (Users usersDb : usersList) {
            if (usersDb.getEmail().equals(user.getEmail())) {
               if (!usersDb.getId().equals(user.getId())) {
                   throw new Conflict("Email address already exists");
                }
            }

        }
        try {
            Users userFromDB = usersRepository.findById(id).orElse(null);
            if (userFromDB != null) {
                return usersRepository.save(user);
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the User
        throw new ResourceNotFound("Could not locate a User with the id: " + id);
    }

    @Override
    public void deleteUser(Long id) {

        try {
            if (usersRepository.existsById(id)) {
                usersRepository.deleteById(id);
                return;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the User
        throw new ResourceNotFound("Could not locate a User with the id: " + id);
    }

    @Override
    public void deleteAllUsers() {
        usersRepository.deleteAll();
    }

    public boolean isPathIdEqualToExistingUserId(Users user) {
        List<Users> usersList = usersRepository.findAll();
        for (Users usersDb : usersList) {
            if (usersDb.getId().equals(user.getId())) {
                return true;
            }
        }
        return false;
    }


}
