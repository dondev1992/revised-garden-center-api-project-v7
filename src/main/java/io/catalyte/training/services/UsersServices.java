package io.catalyte.training.services;

import io.catalyte.training.entities.Users;

import java.util.List;

public interface UsersServices {
    List<Users> getAllUsers(Users user);

    Users getUserById(Long id);

    Users addUser(Users user);

    Users updateUserById(Long id, Users user);

    void deleteUser(Long id);

    void deleteAllUsers();
}
