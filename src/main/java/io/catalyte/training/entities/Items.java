package io.catalyte.training.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

@Entity
public class Items {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @Valid
    @JoinColumn(name = "productId")
    private Products products;

    @Min(value = 1)
    @NotNull(message = "quantity is required")
    private Integer quantity;

    public Items() {
    }

    public Items(Products products, Integer quantity) {
        this.products = products;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Items{" +
                "id=" + id +
                ", products=" + products +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Items items)) return false;
        return Objects.equals(getId(), items.getId()) && Objects.equals(getProducts(), items.getProducts()) && getQuantity().equals(items.getQuantity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProducts(), getQuantity());
    }
}
