package io.catalyte.training.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

import java.util.*;

@Entity
@Table(name = "customers")
public class Customers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "customer name is required")
    private String name;
    @NotBlank(message = "customer email is required")
    @Email(regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "A valid email is required")
    private String email;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    private Address address;

    @JsonIgnore
    @OneToMany(mappedBy = "customers", cascade = CascadeType.ALL)
    private List<Orders> orders;

    public Customers() {
    }

    public Customers(String name, String email, Address address, List<Orders> orders) {
        this.name = name;
        this.email = email;
        this.address = address;

        this.orders = orders;
    }

    public Customers(String name, String email, Address address) {
        this.name = name;
        this.email = email;
        this.address = address;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Orders> getOrders() {
        return orders;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Customers{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", orders=" + orders +
                '}';
    }



}
