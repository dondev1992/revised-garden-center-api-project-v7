package io.catalyte.training.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.OptBoolean;
import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
//    @NotNull
    @Valid
    @ManyToOne(fetch = FetchType.EAGER)
    private Customers customers;
    @JsonFormat(pattern = "yyyy-MM-dd", lenient = OptBoolean.FALSE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    @OneToMany(targetEntity=Items.class, cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "orders_id", referencedColumnName = "id")
    @Valid
    private List<Items> items = new ArrayList<>();
    @Transient
//    @NotNull(message = "order total is required")
    private BigDecimal orderTotal;

    public Orders() {
    }

    public Orders(Customers customers, Date date, List<Items> items) {
        this.customers = customers;
        this.date = date;
        this.items = items;
    }

    public Orders(Customers customers, Date date) {
        this.customers = customers;
        this.date = date;
    }

    public Orders(Date date, BigDecimal orderTotal) {
        this.date = date;
        this.orderTotal = orderTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public BigDecimal getOrderTotal() {
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal itemTotal = BigDecimal.ZERO;
        for (Items item: items) {
            itemTotal = item.getProducts().getPrice().multiply(new BigDecimal(item.getQuantity()));
            total = total.add(itemTotal);
        }
        return total;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", customers=" + customers +
                ", date=" + date +
                ", items=" + items +
                ", orderTotal=" + orderTotal +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Orders Orders = (Orders) o;
        return Objects.equals(id, Orders.id) &&
                Objects.equals(customers, Orders.customers)&&
                Objects.equals(date, Orders.date) &&
                Objects.equals(items, Orders.items) &&
                Objects.equals(orderTotal, Orders.orderTotal);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return Objects.isNull(id) &&
                Objects.isNull(customers) &&
                Objects.isNull(date) &&
                Objects.isNull(items) &&
                Objects.isNull(orderTotal);
    }

}