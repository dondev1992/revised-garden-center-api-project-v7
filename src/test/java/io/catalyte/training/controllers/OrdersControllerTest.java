package io.catalyte.training.controllers;

import io.catalyte.training.repositories.OrdersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class OrdersControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();
    ResultMatcher dataErrorStatus = MockMvcResultMatchers.status().isServiceUnavailable();
    ResultMatcher serverErrorStatus = MockMvcResultMatchers.status().isInternalServerError();
    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private OrdersRepository ordersRepository;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

    }

    @Test
    public void getOrdersReturnsAtLeastTwo() throws Exception {
        mockMvc
                .perform(get("/orders"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(2))));
    }

    @Test
    public void getOrderThatDoesExistByCustomerId() throws Exception {
        mockMvc
                .perform(get("/orders/customer")
                        .param("customerId", String.valueOf(1)))
                .andExpect(okStatus)
                .andExpect(expectedType);
    }

    @Test
    public void getOrderThatDoesExistByProductId() throws Exception {
        mockMvc
                .perform(get("/orders/product")
                        .param("productId", String.valueOf(5)))
                .andDo(print())
                .andExpect(okStatus)
                .andExpect(expectedType);
    }

    @Test
    public void getOrderThatDoesExistByQuantity() throws Exception {
        mockMvc
                .perform(get("/orders/quantity")
                .param("quantity", String.valueOf(12)))
                .andDo(print())
                .andExpect(okStatus)
                .andExpect(expectedType);
    }

    @Test
    public void getOrderThatDoesExistById() throws Exception {
        mockMvc
                .perform(get("/orders/1"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.date", is("2022-10-17")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void postNewOrder() throws Exception {
        String json = "{\"customers\":{\"id\":5,\"name\":\"DI\",\"email\":\"di@email.com\",\"address\":{\"street\":\"1987 Graham St.\",\"city\":\"Charlotte\",\"state\":\"NC\",\"zipCode\":28364}},\"date\":\"2023-12-12\",\"items\":[{\"products\":{\"id\":1,\"sku\":49511,\"type\":\"safety\",\"name\":\"gloves\", \"description\":\"Durable, ergonomic design\", \"manufacturer\":\"Bully Tools\",\"price\":\"12.49\"},\"quantity\":5}], \"orderTotal\":\"78.35\"}";
        this.mockMvc
                .perform(post("/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.date", is("2023-12-12")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void putOrder() throws Exception {
        String json = "{\"id\": 1, \"customers\":{\"id\":5,\"name\":\"DI\",\"email\":\"di@email.com\",\"address\":{\"street\":\"1987 Graham St.\",\"city\":\"Charlotte\",\"state\":\"NC\",\"zipCode\":28364}},\"date\":\"2001-02-03\",\"items\":[{\"products\":{\"id\":1,\"sku\":49511,\"type\":\"safety\",\"name\":\"gloves\", \"description\":\"Durable, ergonomic design\", \"manufacturer\":\"Bully Tools\",\"price\":\"12.49\"},\"quantity\":5}], \"orderTotal\":\"78.35\"}";
        this.mockMvc
                .perform(put("/orders/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.date", is("2001-02-03")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void deleteOrder() throws Exception {
        mockMvc
                .perform(delete("/orders/1"))
                .andExpect(deletedStatus);
    }

    @Test
    public void deleteAllOrders() throws Exception {
        mockMvc
                .perform(delete("/orders"))
                .andExpect(deletedStatus);
    }

    @Test
    public void getOrderBadRequest() throws Exception {
        mockMvc
                .perform(get("/garbage"))
                .andExpect(notFoundStatus);
    }

    @Test
    public void getOrderThatDoesNotExist() throws Exception {
        mockMvc
                .perform(get("/4567890"))
                .andExpect(notFoundStatus);
    }

}