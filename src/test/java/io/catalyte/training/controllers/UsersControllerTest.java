package io.catalyte.training.controllers;

import io.catalyte.training.entities.Users;
import io.catalyte.training.repositories.UsersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class UsersControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();
    ResultMatcher dataErrorStatus = MockMvcResultMatchers.status().isServiceUnavailable();
    ResultMatcher serverErrorStatus = MockMvcResultMatchers.status().isInternalServerError();
    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private UsersRepository usersRepository;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

    }

    @Test
    public void getUsersReturnsAtLeastTwo() throws Exception {
        mockMvc
                .perform(get("/users"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(2))));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void getUserThatDoesExistById() throws Exception {
        mockMvc
                .perform(get("/users/1"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Rachel Monroe")));
    }


    @Test
    public void postNewUser() throws Exception {
        String json = "{\"name\":\"Freddie\",\"title\":\"Lead Programmer\",\"roles\":[\"EMPLOYEE\", \"ADMIN\"], \"email\":\"freddie@email.com\", \"password\":\"liveLife09\"}";
        this.mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Freddie")));
    }



    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void putUser() throws Exception {
        String json = "{\"id\":3,\"name\":\"Joshua\",\"title\":\"Senior Programmer\",\"roles\":[\"EMPLOYEE\"], \"email\":\"joshua@email.com\", \"password\":\"NeverbaCkdOwN340\"}";
        this.mockMvc
                .perform(put("/users/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Joshua")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void deleteUser() throws Exception {
        mockMvc
                .perform(delete("/users/3"))
                .andExpect(deletedStatus);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void deleteAllUsers() throws Exception {
        mockMvc
                .perform(delete("/users"))
                .andExpect(deletedStatus);
    }

    @Test
    public void getUserBadRequest() throws Exception {
        mockMvc
                .perform(get("/users/garbage"))
                .andExpect(badRequestStatus);
    }

    @Test
    public void getUserThatDoesNotExist() throws Exception {
        mockMvc
                .perform(get("/users/4567890"))
                .andExpect(notFoundStatus);
    }


}