package io.catalyte.training.controllers;

import io.catalyte.training.entities.Address;
import io.catalyte.training.entities.Customers;
import io.catalyte.training.repositories.CustomersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class CustomersControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();
    ResultMatcher dataErrorStatus = MockMvcResultMatchers.status().isServiceUnavailable();
    ResultMatcher serverErrorStatus = MockMvcResultMatchers.status().isInternalServerError();
    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private CustomersRepository customersRepository;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

    }

    @Test
    public void getCustomersReturnsAtLeastTwo() throws Exception {
        mockMvc
                .perform(get("/customers"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(2))));
    }

    @Test
    public void getCustomerThatDoesExistById() throws Exception {
        mockMvc
                .perform(get("/customers/1"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Churina Sa")));
    }

    @Test
    public void getCustomerThatDoesExistByStreet() throws Exception {
        mockMvc
                .perform(get("/customers/street")
                        .param("street", "123 Tryon St."))
                .andDo(print())
                .andExpect(okStatus)
                .andExpect(expectedType);

    }

    @Test
    public void getCustomerThatDoesExistByCity() throws Exception {
        mockMvc
                .perform(get("/customers/city")
                        .param("city", "Charlotte"))
                .andDo(print())
                .andExpect(okStatus)
                .andExpect(expectedType);

    }

    @Test
    public void getCustomerThatDoesExistByState() throws Exception {
        mockMvc
                .perform(get("/customers/state")
                        .param("state", "SC"))
                .andDo(print())
                .andExpect(okStatus)
                .andExpect(expectedType);

    }

    @Test
    public void getCustomerThatDoesExistByZipCode() throws Exception {
        mockMvc
                .perform(get("/customers/zipcode")
                        .param("zipCode", String.valueOf(28227)))
                .andDo(print())
                .andExpect(okStatus)
                .andExpect(expectedType);

    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void postNewCustomer() throws Exception {
        String json = "{\"name\":\"Japhonzo\",\"email\":\"japhonzo@email.com\",\"address\":{\"street\":\"987 Graham St.\", \"city\":\"McAllen\",\"state\":\"TX\",\"zipCode\":48364}}";
//        new Customers("Michael", "Lead Programmer", List.of("EMPLOYEE", "ADMIN"), "michael@email.com", "liveLife09"));
        this.mockMvc
                .perform(post("/customers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Japhonzo")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void putCustomer() throws Exception {
        String json = "{\"id\":4,\"name\":\"Alphonzo\",\"email\":\"alphonzo@email.com\",\"address\":{\"street\":\"1232 Rio Grande Ave.\", \"city\":\"McAllen\",\"state\":\"TX\",\"zipCode\":95203}}";
        this.mockMvc
                .perform(put("/customers/4")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Alphonzo")));
    }


    @Test
    public void deleteCustomer() throws Exception {
        Customers customer = new Customers("Japhonzo", "japhonzo@email.com", new Address("987 Graham St.", "McAllen", "TX", 48364));
        customersRepository.save(customer);
        mockMvc
                .perform(delete("/customers/3"))
                .andExpect(deletedStatus);
    }

    @Test
    public void deleteAllCustomers() throws Exception {
//        Customers customer1 = new Customers("Japhonzo", "japhonzo@email.com", new Address("987 Graham St.", "McAllen", "TX", 48364));
//        Customers customer2 = new Customers("Martin", "martin@email.com", new Address("987 Graham St.", "McAllen", "TX", 48364));
//        customersRepository.save(customer1);
//        customersRepository.save(customer2);
        mockMvc
                .perform(delete("/customers"))
                .andExpect(deletedStatus);
    }

    @Test
    public void getCustomerBadRequest() throws Exception {
        mockMvc
                .perform(get("/customers/trash"))
                .andExpect(badRequestStatus);
    }

    @Test
    public void getCustomerThatDoesNotExist() throws Exception {
        mockMvc
                .perform(get("/customers/4567890"))
                .andExpect(notFoundStatus);
    }

}