package io.catalyte.training.controllers;

import io.catalyte.training.entities.Products;
import io.catalyte.training.entities.Users;
import io.catalyte.training.repositories.ProductsRepository;
import io.catalyte.training.repositories.UsersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

//@DirtiesContext
@SpringBootTest
@AutoConfigureMockMvc
class ProductsControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();
    ResultMatcher dataErrorStatus = MockMvcResultMatchers.status().isServiceUnavailable();
    ResultMatcher serverErrorStatus = MockMvcResultMatchers.status().isInternalServerError();
    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private ProductsRepository productsRepository;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

    }

    @Test
    public void getProductsReturnsAtLeastTwo() throws Exception {
        mockMvc
                .perform(get("/products"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(2))));
    }

    @Test
    public void getProductThatDoesExistById() throws Exception {
        mockMvc
                .perform(get("/products/1"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("gloves")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void postNewProduct() throws Exception {
        String json = "{\"sku\":49511,\"type\":\"safety\",\"name\":\"gloves\", \"description\":\"Durable, ergonomic design\",  \"manufacturer\": \"Bully Tools\",\"price\":\"12.49\"}";
        this.mockMvc
                .perform(post("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("gloves")));
    }

    @Test
    public void putProduct() throws Exception {
        String json = "{\"id\":4,\"sku\":497432,\"type\":\"safety\",\"name\":\"gloves\", \"description\":\"Durable, ergonomic design\", \"manufacturer\": \"Bully Tools\",\"price\":\"12.49\"}";
        this.mockMvc
                .perform(put("/products/4")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.type", is("safety")));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void deleteProduct() throws Exception {
        Products product = new Products(7L,498835, "tool", "spade", "Durable, ergonomic design", "Stoller", new BigDecimal("12.49"));
        productsRepository.save(product);
        mockMvc
                .perform(delete("/products/7"))
                .andExpect(deletedStatus);
    }

    @Test
    public void getProductBadRequest() throws Exception {
        mockMvc
                .perform(get("/garbage"))
                .andExpect(notFoundStatus);
    }

    @Test
    public void getProductThatDoesNotExist() throws Exception {
        mockMvc
                .perform(get("/4567890"))
                .andExpect(notFoundStatus);
    }

}