package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.*;
import io.catalyte.training.repositories.OrdersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doThrow;

class OrdersServicesImplementsTest {

    @Mock
    OrdersRepository ordersRepository;

    @InjectMocks
    OrdersServicesImplements ordersServicesImplements;

    Orders testOrder;
    List<Orders> testOrdersList = new ArrayList<>();

    List<Items> itemsList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        // set up test Order
        Customers customer = new Customers("DI", "di@email.com", new Address("987 Graham St.", "Charlotte", "NC", 28364));
//        customer.setId(1L);
        Products product = new Products(496330, "tool", "loppers", "Made in the USA.", "Stoller", new BigDecimal("26.99"));
        product.setId(1L);
        Products product2 = new Products(496330, "tool", "loppers", "Made in the USA.", "Stoller", new BigDecimal("26.99"));
        product.setId(2L);
        itemsList.add(new Items(product, 1));
        itemsList.add(new Items(product2, 5));
        testOrder = new Orders(customer, Date.from(
                LocalDate.parse("2022-11-06").atStartOfDay(ZoneId.systemDefault()).toInstant()), itemsList
        );
        testOrder.setId(1L);

        // set up test list
        testOrdersList.add(testOrder);

        when(ordersRepository.findById(any(Long.class))).thenReturn(Optional.of(testOrdersList.get(0)));
        when(ordersRepository.saveAll(anyCollection())).thenReturn(testOrdersList);
        when(ordersRepository.save(any(Orders.class))).thenReturn(testOrdersList.get(0));
        when(ordersRepository.findAll()).thenReturn(testOrdersList);
        when(ordersRepository.findAll(any(Example.class))).thenReturn(testOrdersList);
        when(ordersRepository.findByItems_ProductsId(any(Long.class))).thenReturn(testOrdersList);
        when(ordersRepository.findByCustomersId(any(Long.class))).thenReturn(testOrdersList);
        when(ordersRepository.findByItems_Quantity(any(Integer.class))).thenReturn(testOrdersList);
    }

    @Test
    void getAllOrders() {
        List<Orders> result = ordersServicesImplements.getAllOrders(new Orders());
        assertEquals(testOrdersList, result);

    }

    @Test
    public void getAllOrdersWithSample() {
        List<Orders> result = ordersServicesImplements.getAllOrders(testOrder);
        assertEquals(testOrdersList, result);
    }

    @Test
    public void getAllOrdersDBError() {
        when(ordersRepository.findAll(any(Example.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.getAllOrders(new Orders()));
    }


    @Test
    public void getOrderById() {
        Orders result = ordersServicesImplements.getOrderById(1L);
        assertEquals(testOrder, result);
    }
    @Test
    public void getAllOrdersByProductId() {
        List<Orders> result = ordersServicesImplements.getAllOrdersByProductId(1L);
        assertEquals(testOrdersList, result);
    }

    @Test
    public void getOrderByProductIdNotFound() {
        when(ordersRepository.findByItems_ProductsId(anyLong())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> ordersServicesImplements.getAllOrdersByProductId(1L));
        String expectedMessage = "Could not locate a Order with the product id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getOrderByProductIdDBError() {
        when(ordersRepository.findByItems_ProductsId(anyLong())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.getAllOrdersByProductId(1L));
    }

    @Test
    public void getAllOrdersByCustomerId() {
        List<Orders> result = ordersServicesImplements.getAllOrdersByCustomerId(1L);
        assertEquals(testOrdersList, result);
    }

    @Test
    public void getOrderByCustomerIdNotFound() {
        when(ordersRepository.findByCustomersId(anyLong())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> ordersServicesImplements.getAllOrdersByCustomerId(1L));
        String expectedMessage = "Could not locate a Order with the customer id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getOrderByCustomerIdDBError() {
        when(ordersRepository.findByCustomersId(anyLong())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.getAllOrdersByCustomerId(1L));
    }

    @Test
    public void getAllOrdersByQuantity() {
        List<Orders> result = ordersServicesImplements.getAllOrdersByQuantity(5);
        assertEquals(testOrdersList, result);
    }

    @Test
    public void getOrderByQuantityNotFound() {
        when(ordersRepository.findByItems_Quantity(anyInt())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> ordersServicesImplements.getAllOrdersByQuantity(5));
        String expectedMessage = "Could not locate a Order with the quantity: 5";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getOrderByQuantityDBError() {
        when(ordersRepository.findByItems_Quantity(anyInt())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.getAllOrdersByQuantity(5));
    }

    @Test
    public void getOrderByIdNotFound() {
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.empty());
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> ordersServicesImplements.getOrderById(1L));
        String expectedMessage = "Could not locate a Order with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getOrderByIdDBError() {
        when(ordersRepository.findById(any(Long.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.getOrderById(1L));
    }

    @Test
    public void addOrder() {
        Orders result = ordersServicesImplements.addOrder(testOrder);
        assertEquals(testOrder, result);

    }


    @Test
    public void addOrderDBError() {
        when(ordersRepository.save(any(Orders.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.addOrder(testOrder));
    }


    @Test
    void updateOrderById() {
        Orders result = ordersServicesImplements.updateOrderById(1L, testOrder);
        assertEquals(testOrder, result);

    }

    @Test
    public void updateOrderByIdDBError() {
        when(ordersRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.updateOrderById(1L, testOrder));
    }

    @Test
    public void updateOrderByIdNotFound() {
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFound.class,
                () -> ordersServicesImplements.updateOrderById(1L, testOrder));
        String expectedMessage = "Could not locate a Order with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void updateOrderByIdBadData() {
        Exception exception = assertThrows(BadDataResponse.class,
                () -> ordersServicesImplements.updateOrderById(2L, testOrder));
        String expectedMessage = "Order id must match the id specified in the URL";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void deleteOrder() {
        when(ordersRepository.existsById(anyLong())).thenReturn(true);
        ordersServicesImplements.deleteOrder(1L);
        verify(ordersRepository).deleteById(any());

    }

    @Test
    public void deleteOrderBadID() {
        doThrow(new ResourceNotFound("Database unavailable")).when(ordersRepository)
                .deleteById(anyLong());
        assertThrows(ResourceNotFound.class,
                () -> ordersServicesImplements.deleteOrder(1L));
    }

    @Test
    public void deleteOrderDBError() {
        doThrow(new ServiceUnavailable("Database unavailable")).when(ordersRepository)
                .existsById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> ordersServicesImplements.deleteOrder(1L));
    }

}