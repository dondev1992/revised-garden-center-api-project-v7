package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.Conflict;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Products;
import io.catalyte.training.repositories.ProductsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doThrow;

class ProductsServicesImplementsTest {

    @Mock
    ProductsRepository productsRepository;

    @InjectMocks
    ProductsServicesImplements productsServicesImplements;

    Products testProduct;
    Products testProduct2;
    List<Products> testProductsList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        // set up test Product
        testProduct2 = new Products(490222,"tool", "fork", "Extra reinforced handles.", "Bully Tools", new BigDecimal("22.79"));
        testProduct = new Products(493333,"tool", "fork", "Extra reinforced handles.", "Bully Tools", new BigDecimal("22.79"));
        testProduct.setId(7L);

        // set up test list
        testProductsList.add(testProduct);

        when(productsRepository.findById(any(Long.class))).thenReturn(Optional.of(testProductsList.get(0)));
        when(productsRepository.saveAll(anyCollection())).thenReturn(testProductsList);
        when(productsRepository.save(any(Products.class))).thenReturn(testProductsList.get(0));
        when(productsRepository.findAll()).thenReturn(testProductsList);
        when(productsRepository.findAll(any(Example.class))).thenReturn(testProductsList);
    }

    @Test
    void getAllProducts() {
        List<Products> result = productsServicesImplements.getAllProducts(new Products());
        assertEquals(testProductsList, result);

    }

    @Test
    public void getAllProductsWithSample() {
        List<Products> result = productsServicesImplements.getAllProducts(testProduct);
        assertEquals(testProductsList, result);
    }

    @Test
    public void getProductById() {
        Products result = productsServicesImplements.getProductById(1L);
        assertEquals(testProduct, result);
    }

    @Test
    public void getProductByIdNotFound() {
        when(productsRepository.findById(anyLong())).thenReturn(Optional.empty());
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> productsServicesImplements.getProductById(7L));
        String expectedMessage = "Could not locate a Product with the id: 7";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
//    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void addProduct() {
        Products result = productsServicesImplements.addProduct(testProduct2);
        assertEquals(testProduct, result);

    }


    @Test
    public void addProductDBError() {
        when(productsRepository.save(any(Products.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(Conflict.class,
                () -> productsServicesImplements.addProduct(testProduct));
    }


    @Test
    void updateProductById() {
        Products result = productsServicesImplements.updateProductById(7L, testProduct);
        assertEquals(testProduct, result);

    }

    @Test
    public void updateProductByIdDBError() {
        when(productsRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> productsServicesImplements.updateProductById(7L, testProduct));
    }

    @Test
    public void updateProductByIdNotFound() {
        when(productsRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFound.class,
                () -> productsServicesImplements.updateProductById(7L, testProduct));
        String expectedMessage = "Could not locate a Product with the id: 7";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void updateProductByIdBadData() {
        Exception exception = assertThrows(BadDataResponse.class,
                () -> productsServicesImplements.updateProductById(2L, testProduct));
        String expectedMessage = "Product id must match the id specified in the URL";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void deleteProduct() {
        when(productsRepository.existsById(anyLong())).thenReturn(true);
        productsServicesImplements.deleteProduct(7L);
        verify(productsRepository).deleteById(any());

    }

    @Test
    public void deleteProductBadID() {
        doThrow(new ResourceNotFound("Database unavailable")).when(productsRepository)
                .deleteById(anyLong());
        assertThrows(ResourceNotFound.class,
                () -> productsServicesImplements.deleteProduct(7L));
    }

    @Test
    public void deleteProductDBError() {
        doThrow(new ServiceUnavailable("Database unavailable")).when(productsRepository)
                .existsById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> productsServicesImplements.deleteProduct(7L));
    }

}