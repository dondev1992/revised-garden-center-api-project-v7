package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.Conflict;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Address;
import io.catalyte.training.entities.Customers;
import io.catalyte.training.entities.Orders;
import io.catalyte.training.repositories.CustomersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doThrow;

class CustomersServicesImplementsTest {

    @Mock
    CustomersRepository customersRepository;

    @InjectMocks
    CustomersServicesImplements customersServicesImplements;

    // variables for expected results
    Customers testCustomer;
    Customers testCustomer2;
    List<Customers> testCustomersList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        // set up test User
        testCustomer2 = new Customers("Donnie", "donnie@email.com", new Address("4567 Trade St.",  "Charlotte", "NC", 28202));
        testCustomer = new Customers("Donnie", "kynna@email.com", new Address("4567 Trade St.",  "Charlotte", "NC", 28202));
        testCustomer.setId(1L);

        // set up test list
        testCustomersList.add(testCustomer);

        when(customersRepository.findById(any(Long.class))).thenReturn(Optional.of(testCustomersList.get(0)));
        when(customersRepository.saveAll(anyCollection())).thenReturn(testCustomersList);
        when(customersRepository.save(any(Customers.class))).thenReturn(testCustomersList.get(0));
        when(customersRepository.findAll()).thenReturn(testCustomersList);
        when(customersRepository.findAll(any(Example.class))).thenReturn(testCustomersList);
        when(customersRepository.findByAddressStreet(anyString())).thenReturn(testCustomersList);
        when(customersRepository.findByAddressCity(anyString())).thenReturn(testCustomersList);
        when(customersRepository.findByAddressState(anyString())).thenReturn(testCustomersList);
        when(customersRepository.findByAddressZipCode(anyInt())).thenReturn(testCustomersList);
    }

    @Test
    void getAllCustomers() {
        List<Customers> result = customersServicesImplements.getAllCustomers(new Customers());
        assertEquals(testCustomersList, result);

    }

    @Test
    public void getAllCustomersWithSample() {
        List<Customers> result = customersServicesImplements.getAllCustomers(testCustomer);
        assertEquals(testCustomersList, result);
    }


    @Test
    public void getCustomerById() {
        Customers result = customersServicesImplements.getCustomerById(1L);
        assertEquals(testCustomer, result);
    }

    @Test
    public void getCustomerByIdNotFound() {
        when(customersRepository.findById(anyLong())).thenReturn(Optional.empty());
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.getCustomerById(1L));
        String expectedMessage = "Could not locate a Customer with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getAllCustomersByStreet() {
        List<Customers> result = customersServicesImplements.getCustomersByStreet("4567 Trade St.");
        assertEquals(testCustomersList, result);
    }

    @Test
    public void getAllCustomersByStreetNotFound() {
        when(customersRepository.findByAddressStreet(anyString())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.getCustomersByStreet("4567 Trade St."));
        String expectedMessage = "Could not locate any Customers with the street: 4567 Trade St.";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getAllCustomersByStreetDBError() {
        when(customersRepository.findByAddressStreet(anyString())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.getCustomersByStreet("4567 Trade St."));
    }

    @Test
    public void getAllCustomersByCity() {
        List<Customers> result = customersServicesImplements.getCustomersByCity("Charlotte");
        assertEquals(testCustomersList, result);
    }

    @Test
    public void getAllCustomersByCityNotFound() {
        when(customersRepository.findByAddressCity(anyString())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.getCustomersByCity("Charlotte"));
        String expectedMessage = "Could not locate any Customers with the city: Charlotte";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getAllCustomersByCityDBError() {
        when(customersRepository.findByAddressCity(anyString())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.getCustomersByCity("Charlotte"));
    }

    @Test
    public void getAllCustomersByState() {
        List<Customers> result = customersServicesImplements.getCustomersByState("NC");
        assertEquals(testCustomersList, result);
    }

    @Test
    public void getAllCustomersByStateNotFound() {
        when(customersRepository.findByAddressState(anyString())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.getCustomersByState("NC"));
        String expectedMessage = "Could not locate any Customers with the state: NC";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getAllCustomersByStateDBError() {
        when(customersRepository.findByAddressState(anyString())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.getCustomersByState("NC"));
    }

    @Test
    public void getAllCustomersByZipCode() {
        List<Customers> result = customersServicesImplements.getCustomersByZipCode(28202);
        assertEquals(testCustomersList, result);
    }

    @Test
    public void getAllCustomersByZipCodeNotFound() {
        when(customersRepository.findByAddressZipCode(anyInt())).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.getCustomersByZipCode(28202));
        String expectedMessage = "Could not locate any Customers with the zipCode: 28202";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void getAllCustomersByZipCodeDBError() {
        when(customersRepository.findByAddressZipCode(anyInt())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.getCustomersByZipCode(28202));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void addCustomer() {
        Customers result = customersServicesImplements.addCustomer(testCustomer);
        assertEquals(testCustomer, result);

    }


    @Test
    public void addCustomerDBError() {
        when(customersRepository.save(any(Customers.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.addCustomer(testCustomer));
    }


    @Test
    void updateCustomerById() {
        Customers result = customersServicesImplements.updateCustomerById(1L, testCustomer);
        assertEquals(testCustomer, result);

    }

    @Test
    public void updateCustomerByIdDBError() {
        when(customersRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.updateCustomerById(1L, testCustomer));
    }

    @Test
    public void updateCustomerByIdNotFound() {
        when(customersRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.updateCustomerById(1L, testCustomer));
        String expectedMessage = "Could not locate a Customer with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void updateCustomerByIdBadData() {
        Exception exception = assertThrows(BadDataResponse.class,
                () -> customersServicesImplements.updateCustomerById(2L, testCustomer));
        String expectedMessage = "Customer id must match the id specified in the URL";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void deleteCustomer() {
        when(customersRepository.existsById(anyLong())).thenReturn(true);
        customersServicesImplements.deleteCustomer(1L);
        verify(customersRepository).deleteById(any());

    }

    @Test
    public void deleteCustomerBadID() {
        doThrow(new ResourceNotFound("Database unavailable")).when(customersRepository)
                .deleteById(anyLong());
        assertThrows(ResourceNotFound.class,
                () -> customersServicesImplements.deleteCustomer(1L));
    }

    @Test
    public void deleteCustomerDBError() {
        doThrow(new ServiceUnavailable("Database unavailable")).when(customersRepository)
                .existsById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> customersServicesImplements.deleteCustomer(1L));
    }

}