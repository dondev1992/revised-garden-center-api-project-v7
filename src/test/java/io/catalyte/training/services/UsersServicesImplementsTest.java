package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.Conflict;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Users;
import io.catalyte.training.repositories.UsersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class UsersServicesImplementsTest {

    @Mock
    UsersRepository usersRepository;

    @InjectMocks
    UsersServicesImplements usersServicesImplements;

    // variables for expected results
    Users testUser;
    Users newUser;
    List<Users> testUsersList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        // set up test Users
        testUser = new Users("Sonya", "Cashier", List.of("EMPLOYEE"), "sonya@email.com", "happyDays2023");
        testUser.setId(1L);
        newUser = new Users("Richard Jefferson", "Cashier", List.of("EMPLOYEE"), "richard@email.com", "richBoYjeFF100");

        // set up test list
        testUsersList.add(testUser);
        testUsersList.add(newUser);

        when(usersRepository.findById(any(Long.class))).thenReturn(Optional.of(testUsersList.get(0)));
        when(usersRepository.saveAll(anyCollection())).thenReturn(testUsersList);
        when(usersRepository.save(any(Users.class))).thenReturn(testUsersList.get(0));
        when(usersRepository.findAll()).thenReturn(testUsersList);
        when(usersRepository.findAll(any(Example.class))).thenReturn(testUsersList);
    }

    @Test
    void getAllUsers() {
        List<Users> result = usersServicesImplements.getAllUsers(new Users());
        assertEquals(testUsersList, result);

    }

    @Test
    public void getAllUsersWithSample() {
        List<Users> result = usersServicesImplements.getAllUsers(testUser);
        assertEquals(testUsersList, result);
    }

    @Test
    public void getUserById() {
        Users result = usersServicesImplements.getUserById(1L);
        assertEquals(testUser, result);
    }

    @Test
    public void getUserByIdNotFound() {
        when(usersRepository.findById(anyLong())).thenReturn(Optional.empty());
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> usersServicesImplements.getUserById(1L));
        String expectedMessage = "Could not locate a User with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void addUser() {
        Users result = usersServicesImplements.addUser(testUser);
        assertEquals(testUser, result);

    }


    @Test
    public void addUserDBError() {
        when(usersRepository.save(any(Users.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> usersServicesImplements.addUser(testUser));
    }


    @Test
    void updateUserById() {
        Users result = usersServicesImplements.updateUserById(1L, testUser);
        assertEquals(testUser, result);

    }

    @Test
    public void updateUserByIdDBError() {
        when(usersRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> usersServicesImplements.updateUserById(1L, testUser));
    }

    @Test
    public void updateUserByIdNotFound() {
        when(usersRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFound.class,
                () -> usersServicesImplements.updateUserById(1L, testUser));
        String expectedMessage = "Could not locate a User with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void updateUserByIdBadData() {
        Exception exception = assertThrows(BadDataResponse.class,
                () -> usersServicesImplements.updateUserById(2L, testUser));
        String expectedMessage = "User id must match the id specified in the URL";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void deleteUser() {
        when(usersRepository.existsById(anyLong())).thenReturn(true);
        usersServicesImplements.deleteUser(1L);
        verify(usersRepository).deleteById(any());

    }

    @Test
    public void deleteUserBadID() {
        doThrow(new ResourceNotFound("Database unavailable")).when(usersRepository)
                .deleteById(anyLong());
        assertThrows(ResourceNotFound.class,
                () -> usersServicesImplements.deleteUser(1L));
    }

    @Test
    public void deleteUserDBError() {
        doThrow(new ServiceUnavailable("Database unavailable")).when(usersRepository)
                .existsById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> usersServicesImplements.deleteUser(1L));
    }

}